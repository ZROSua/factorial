﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SilniA
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("For starting recursion press 1, for starting iteration press 2");
            Factorial factorial = new Factorial();

            ConsoleKeyInfo key;
            {
                key = Console.ReadKey();
                string button = "" + key.KeyChar;

                if (button == "2")
                {
                    Console.WriteLine("\nPress Enter");
                    while (true)
                    {
                        key = Console.ReadKey();
                        button = "" + key.Key;
                        if (button == "Enter")
                        {
                            Console.WriteLine("\n");
                            factorial.factorialIteration();
                        }
                    }
                }
                else if (button == "1")
                {
                    Console.WriteLine("\nEnter factorial number");
                    double number;
                    number = Double.Parse(Console.ReadLine());
                    Console.WriteLine("\n");
                    factorial.factorialRecursion(number);
                }
            }
        }

        public class Factorial
        {
            public double counter;
            public int counterIteration = 1;
            public double factorialRecursion(double counter)
            {
                if (counter == 1)
                {
                    Console.WriteLine("1");
                    return 1;
                }
                else
                    this.counter = counter * factorialRecursion(counter - 1);
                Console.WriteLine(this.counter);
                return this.counter;
            }
            


            public void factorialIteration()
            {
                long factorial = 1;
                for (int i = 1; i <= counterIteration; i++)
                {
                    factorial = factorial * i;
                }
                counterIteration = ++counterIteration;
                Console.WriteLine("!" + (counterIteration - 1) + " Factorial " + factorial);
                Console.WriteLine("Press Enter for next iteration");
            }
        }
    }
}
